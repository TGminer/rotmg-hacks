# K-Relay

A modular Realm of the Mad God man-in-the-middle proxy. Developed by KrazyShank/Kronks on MPGH. Maintained by toddddd on MPGH.

Current Version: 27.7.X12.1  |  [MPGH Thread](http://www.mpgh.net/forum/showthread.php?t=1240170)

[Download](https://github.com/rotmg-hacks/hax/raw/master/utils/krelay/27.7.X12_KRelay_mpgh.net.zip)  |  [Patch for 27.7.X12.1](https://github.com/rotmg-hacks/hax/raw/master/utils/krelay/27.7.X12.1_Lib_K_Relay_mpgh.net.zip)

*You will need to patch K-Relay before using it.*

### How to patch
---
1. Download and extract both archives.
2. Extract ```27.7.X12_KRelay_mpgh.net.zip``` into a folder. (e.g. K-Relay)
3. Extract ```Lib K Relay.dll``` from ```27.7.X12.1_Lib_K_Relay_mpgh.net.zip``` into your K-Relay folder.

Congrats. It should now be patched to 27.7.X12.1. Enjoy!
