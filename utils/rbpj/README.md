# Realmbot Projector

A simple app that packs a specified SWF into a neat EXE. *Created by IziLife on MPGH.*

![](https://i.imgur.com/K2Cfz9D.gif)

[**Download**](https://github.com/rotmg-hacks/hax/raw/master/utils/rbpj/RealmBot-Projecter_mpgh.net.zip)  |  [MPGH Thread](www.mpgh.net/forum/showthread.php?t=898359)
