# Flash Projector

This utility allows you to play SWF files.
Important for playing hacked clients.

### Available versions
* Projector 13  |  [Download](https://github.com/rotmg-hacks/hax/raw/master/utils/flash/flashplayer_13_sa.exe)
* Projector 14  |  [Download](https://github.com/rotmg-hacks/hax/raw/master/utils/flash/flashplayer_14_sa.exe)
* Projector 15  |  [Download](https://github.com/rotmg-hacks/hax/raw/master/utils/flash/flashplayer_15_sa.exe)
* Projector 16  |  [Download](https://github.com/rotmg-hacks/hax/raw/master/utils/flash/flashplayer_16_sa.exe)

```
More versions to be uploaded soon.
```
