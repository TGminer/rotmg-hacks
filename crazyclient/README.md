# CrazyClient

A new, made from scratch hacked client made by CrazyJani on MPGH.

Current Version: v8 -- 27.7.X13  |  [MPGH Thread](http://www.mpgh.net/forum/showthread.php?t=1249958)

[**Download** (comes with EXE and SWF)](https://github.com/rotmg-hacks/hax/raw/master/crazyclient/crazyclientv8_mpgh.net.zip)
