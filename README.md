![icon](https://raw.githubusercontent.com/rotmg-hacks/hax/master/img/rotmg-50.png)

# Realm of the Mad God Hacks
I do **NOT** own **ANY** of these hacks. Most of them come from [*MPGH*](http://www.mpgh.net/forum/forumdisplay.php?f=599).


## Available hacks on this repo

### Current RotMG Version: 27.7.X13


* [CrazyClient](https://github.com/rotmg-hacks/hax/tree/master/crazyclient) by CrazyJani
* [myClient](https://github.com/rotmg-hacks/hax/tree/master/myclient) by toddddd
* [Nilly's Reborn](https://github.com/rotmg-hacks/hax/tree/master/nillysreborn) by 059
* [ORape](https://github.com/rotmg-hacks/hax/tree/master/orape) by JustAnoobROTMG and Scellow

### Useful utilities
* [K-Relay](https://github.com/rotmg-hacks/hax/tree/master/utils/krelay)
* [Adobe Flash Projector](https://github.com/rotmg-hacks/hax/tree/master/utils/flash)
* [FlameWare](https://github.com/rotmg-hacks/hax/tree/master/utils/fw) by Alde
* [Realmbot Projector](https://github.com/rotmg-hacks/hax/tree/master/utils/rbpj) by IziLife

---

The repo for Gitter will have to wait, some problems occurred.

**Have a problem? Create an issue and I will get to you ASAP.**
